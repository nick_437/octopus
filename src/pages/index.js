// components
import Main from '../components/Main/Main'
// style
import css from '../styles/Home.module.scss'

export default function Home() {
  return (
    <div className={css.wrapper}>
      <Main />
    </div>
  )
}
