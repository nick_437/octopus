// package
import cn from 'classnames'
// style
import css from './Toolbar.module.scss'

export default function Toolbar({ className, fnArrBlock, id }) {
  return (
    <div className={cn(css.wrapper, className)}>
      <span
        className={css.item}
        onClick={() => fnArrBlock(new Date().getTime(), 'Block name', id)}
      >
        <img
          src='/icon/plus.svg'
          alt='plus'
          className={css.plus}
        />
      </span>
    </div>
  )
}
