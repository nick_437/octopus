// packages
import { useState, useCallback } from 'react'

export default function useBlock() {
  const [hover, setHover] = useState(false) // hover block
  const [value, setValue] = useState('') // value textarea

  const handleChangeValue = useCallback(
    (e) => {
      setValue(e.target.value)
    }, [setValue]
  )

  return {
    hover,
    setHover,
    value,
    handleChangeValue
  }
}
