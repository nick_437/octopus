// package
import cn from 'classnames'
import { Draggable } from 'react-beautiful-dnd'
// hooks
import useBlock from './useBlock'
// style
import css from './Block.module.scss'

export default function Block ({
  className,
  id,
  title,
  activeBlock,
  fnActiveBloc,
  active,
  fnDeleteId,
  index
}) {
  const {
    setHover,
    hover,
    value,
    handleChangeValue
  } = useBlock()
  return (
    <Draggable
      draggableId={`task-${id}`}
      index={index}
    >
      {provided => {
        return (
          <div
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            ref={provided.innerRef}
            className={cn(css.wrapper, className, {
              [css._active]: active,
              [css._activeBlock]: activeBlock === id
            })}
            onClick={() => fnActiveBloc(id)}
            id={id}
          >
            {activeBlock === id
              ? (
                <div className={css.container}>
                  <textarea
                    rows='1'
                    placeholder={value || title}
                    className={css.textarea}
                    onChange={(e) => handleChangeValue(e)}
                  />
                  <button
                    className={cn(css.button, {
                      [css._hover]: hover
                    })}
                    onMouseEnter={() => setHover(true)}
                    onMouseLeave={() => setHover(false)}
                    onClick={() => fnDeleteId(id)}
                  >
                    {hover
                      ? (
                        <img
                          src='/icon/delete_white.svg'
                          alt='delete'
                          className={css.delete}
                        />
                      )
                      : (
                        <img
                          src='/icon/delete.svg'
                          alt='delete'
                          className={css.delete}
                        />
                      )
                    }
                  </button>
                </div>
              )
              : value || title
            }
          </div>
        )
      }}
    </Draggable>
  )
}
