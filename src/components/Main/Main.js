// package
// import cn from 'classnames'
import { DragDropContext } from 'react-beautiful-dnd'
// components
import Column from '../Column/Column'
// hooks
import useMain from './useMain'
// style
import css from './Main.module.scss'

export default function Main() {
  const {
    active,
    handleClickActive,
    arrBlock,
    handlePushArrBlock,
    activeBlock,
    handleActiveBlock,
    handleDeleteId,
    handlePushArrPageRow,
    arrPageRow,
    handleDeleteIdPage,
    setData,
    data,
    handlePushArrBlockPage
  } = useMain()

  // onDragStart
  const start = {
    draggableId: 'list',
    type: 'TYPE',
    source: {
      droppableId: 'column-1',
      index: 0
    }
  }

  // onDrugUpdate
  const update = {
    ...start,
    destination: {
      droppableId: 'column-1',
      index: 1
    }
  }

  // onDragEnd
  const result = {
    ...update,
    reason: 'DROP'
  }

  const onDragStart = start => {

  }

  const onDragUpdate = update => {

  }

  const onDragEnd = result => {
    const {destination, source, draggableId} = result

    if (!destination) {
      return
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return
    }

    const start = data?.columns[source.droppableId]
    const finish = data?.columns[destination.droppableId]

    if (start === finish) {
      const newTaskIds = Array.from(start?.taskIds)
      newTaskIds.splice(source.index, 1)
      newTaskIds.splice(destination.index, 0, draggableId)

      const newColumn = {
        ...start,
        taskIds: newTaskIds
      }

      const newState = {
        ...data,
        columns: {
          ...data?.columns,
          [newColumn?.id]: newColumn
        }
      }

      setData(newState)
      return
    }

    // Moving from one list to another
    const startTaskIds = Array.from(start?.taskIds)
    startTaskIds.splice(source.index, 1)
    const newStart = {
      ...start,
      taskIds: startTaskIds
    }
    console.log(startTaskIds)

    const finishTaskIds = Array.from(finish?.taskIds)
    finishTaskIds.splice(destination.index, 0, draggableId)
    const newFinish = {
      ...finish,
      taskIds: finishTaskIds
    }
    console.log(finishTaskIds)
    const newState = {
      ...data,
      columns: {
        ...data?.columns,
        [newStart?.id]: newStart,
        [newFinish?.id]: newFinish
      }
    }
    console.log(newState)
    setData(newState)

  }

  return (
    <DragDropContext
      onDragStart={onDragStart}
      onDragUpdate={onDragUpdate}
      onDragEnd={onDragEnd}
    >
      {data?.columnOrder?.map(columnId => {
        const column = data.columns[columnId]
        const tasks = column?.taskIds?.map(taskId => data?.tasks[taskId])

        return (
          <Column
            key={column?.id}
            id={column?.id}
            column={column}
            tasks={tasks}
            active={active}
            handleClickActive={handleClickActive}
            activeBlock={activeBlock}
            handleActiveBlock={handleActiveBlock}
            handleDeleteId={handleDeleteId}
            handlePushArrBlock={handlePushArrBlock}
            arrBlock={arrBlock}
            handlePushArrPageRow={handlePushArrPageRow}
          />
        )
      })}
    </DragDropContext>
  )
}
