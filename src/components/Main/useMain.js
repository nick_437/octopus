// packages
import { useCallback, useState, useEffect, useMemo } from 'react'

export default function useMain () {
  // const newData = {
  //   tasks: {
  //     'task-0': {
  //       id: 'task-0',
  //       contant: 'bla-bla-bla'
  //     },
  //     'task-1': {
  //       id: 'task-1',
  //       contant: 'bla-bla-bla'
  //     }
  //   }
  // }
  // console.log(newData)

  // MAIN
  const [data, setData] = useState({}) // start page
  const [active, setActive] = useState(null) // active main
  const [arrBlock, setArrBlock] = useState([]) // array block
  const [activeBlock, setActiveBlock] = useState('') // active block
  const [deleteId, setDeleteId] = useState([]) // delete id block
  const [deleteIdPage, setDeleteIdPage] = useState([]) // delete id page
  const [arrPageRow, setArrPageRow] = useState([]) // array page row
  console.log(arrBlock)
  console.log(active)
  // PAGE
  const [arrBlockPage, setArrBlockPage] = useState([]) // array block page

  // данные для MAIN
  console.log(arrPageRow)
  console.log(deleteId)
  // console.log(deleteIdPage)

  // fn active main || page
  const handleClickActive = useCallback(
    (id) => {
      setActive(id)
    }, [setActive]
  )

  // fn push arr block
  const handlePushArrBlock = useCallback(
    (id, title, idPage) => {
      setArrBlock([
        ...arrBlock,
        {
          idPage: idPage,
          id: id,
          title: title
        }
      ])
    }, [setArrBlock, arrBlock]
  )

  // fn push arr page row
  const handlePushArrPageRow = useCallback(
    (id, title) => {
      setArrPageRow([
        ...arrPageRow,
        {
          id: id,
          title: title
        }
      ])
    }, [setArrPageRow, arrPageRow]
  )

  // active block
  const handleActiveBlock = useCallback(
    (id) => {
      setActiveBlock(id)
    }, [setActiveBlock, setDeleteId]
  )

  // delete arr block
  const handleDeleteId = useCallback(
    (id) => {
      setDeleteId([
        ...deleteId,
        id
      ])
    }, [setDeleteId, deleteId]
  )

  // delete arr page
  const handleDeleteIdPage = useCallback(
    (id) => {
      setDeleteIdPage([
        ...deleteIdPage,
        id
      ])
    }, [setDeleteIdPage, deleteIdPage]
  )

  // new arr page if deleteIdPage > 0
  const newArrPageRow =
    deleteIdPage && deleteIdPage.length > 0
      ? arrPageRow.filter(item => !deleteIdPage.includes(item?.id))
      : arrPageRow

  // данные для PAGE
  // fn push arr block page
  const handlePushArrBlockPage = useCallback(
    (id, title) => {
      setArrBlockPage([
        ...arrBlockPage,
        {
          id: id,
          title: title
        }
      ])
    }, [setArrBlockPage, arrBlockPage]
  )

  // формирование data на основе main + page
  useEffect(
    () => {
      setData(
        {
          // 'tasks':
          //   deleteId && deleteId.length > 0
          //     ? arrBlock.filter(item => !deleteId.includes(item?.id)).reduce((acc, x) => ({
          //       ...acc,
          //       [`task-${x?.id}`]: {
          //         id: x?.id,
          //         title: x?.title
          //       }
          //     }), {})
          //     : arrBlock.reduce((acc, x) => ({
          //       ...acc,
          //       [`task-${x?.id}`]: {
          //         idPage: x?.idPage,
          //         id: x?.id,
          //         title: x?.title
          //       }
          //     }), {}),
          'tasks': arrBlock.reduce((acc, x) => ({
            ...acc,
            [`task-${x?.id}`]: {
              idPage: x?.idPage,
              id: x?.id,
              title: x?.title
            }
          }), {}),
          columns: arrPageRow.reduce((acc, x, key) => ({
            ...acc,
            [`column-${key+2}`]: {
              id: `column-${key+2}`,
              title: x?.title,
              taskIds: arrBlock.filter(item => item?.idPage === `column-${key+2}`).map(item => `task-${item?.id}`)
            }
          }), {
            'column-1': {
              id: 'column-1',
              title: 'Main',
              taskIds: arrBlock.filter(item => item?.idPage === 'column-1').map(item => `task-${item?.id}`)
              // taskIds:
              //   deleteId && deleteId.length > 0
              //     ? arrBlock.filter(item => item?.idPage === 'column-1').filter(item => !deleteId.includes(item?.id)).map(item => `task-${item?.id}`)
              //     : arrBlock.filter(item => item?.idPage === 'column-1').map(item => `task-${item?.id}`)
            }
          }),
          columnOrder: arrPageRow.reduce((acc, x, key) => ([
            ...acc,
            `column-${key+2}`
          ]), ['column-1'])
        }
      )
    }, [setData, arrBlock, arrPageRow, active]
  )

  console.log(arrBlock)
  console.log(data)

  return {
    setData,
    active,
    handleClickActive,
    arrBlock,
    handlePushArrBlock,
    activeBlock,
    handleActiveBlock,
    handleDeleteId,
    handlePushArrPageRow,
    arrPageRow: newArrPageRow,
    handleDeleteIdPage,
    data,
    handlePushArrBlockPage
  }
}
