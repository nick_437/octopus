// package
import cn from 'classnames'
import { Droppable } from 'react-beautiful-dnd'
// components
import Block from '../Block/Block'
import Toolbar from '../Toolbar/Toolbar'
// style
import css from '../Main/Main.module.scss'

export default function Column({
  id,
  column,
  tasks,
  active,
  handleClickActive,
  activeBlock,
  handleActiveBlock,
  handleDeleteId,
  handlePushArrBlock,
  arrBlock,
  handlePushArrPageRow
}) {
  console.log(column)
  console.log(tasks)
  return (
    <div className={css.wrapper_global}>
      {active === id && (
        <Toolbar
          fnArrBlock={handlePushArrBlock}
          id={id}
          className={css.toolbar}
        />
      )}
      <div
        className={cn(css.wrapper, {
          [css._active]: active === id
        })}
      >
        <div className={css.container}>
          <div
            className={css.title}
            onClick={() => handleClickActive(id)}
          >
            {column?.title}
          </div>
          {tasks && tasks.length > 0 && (
            <Droppable droppableId={column.id}>
              {provided => (
                <div
                  ref={provided.innerRef}
                  {...provided.droppableProps}
                >
                  {tasks.map((task, index) => (
                    <Block
                      {...task}
                      key={task?.id}
                      active={active === id}
                      activeBlock={activeBlock}
                      className={css.block}
                      fnActiveBloc={handleActiveBlock}
                      fnDeleteId={handleDeleteId}
                      index={index}
                    />
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          )}
        </div>
        <button
          className={cn(css.button_add__row, {
            [css._active]: active === id
          })}
          onClick={() => handlePushArrPageRow(new Date().getTime(), 'Page')}
        >
          <img
            src='/icon/plus_circle.svg'
            alt='arrow'
            className={css.icon}
          />
        </button>
      </div>
    </div>
  )
}
